#!/bin/bash
cd "$( cd "$( dirname "$0" )" && pwd )"

echo "Bashup started"
TIME=$(date +%s)

if sh ./script/run.sh $DBS_SUBDIR; then
  echo "Bashup finished: SUCCESS"
  echo "Bashup took $(($(date +%s) - TIME)) seconds"
  exit 0
else
  echo "Bashup finished: FAILURE"
  exit 1
fi
