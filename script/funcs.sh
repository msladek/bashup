#!/bin/bash

getPlatform() {
  echo $(uname)
}

trim() {
  trimmed=$1
  trimmed=${trimmed%% }
  trimmed=${trimmed## }
  echo $trimmed
}

getProperty() {
  PROP=$(grep $1 ./bashup.config | cut -d ':' -f 2)
  echo $(trim $PROP)
}

debug() {
  if [ $(getProperty 'DEBUG_MODE') = '0' ]; then
    echo "DEBUG: $1"
  fi
}

warn() {
  echo "WARNING: $1"
}

isSSH() {
  SSH_MODE=$(getProperty 'SSH_MODE')
  if [ $SSH_MODE -ne 0 ]; then
    return 0
  else
    return 1
  fi
}

getSSHCmdPart() {
  echo "ssh -p $(getProperty 'SSH_PORT') -i $(getProperty 'SSH_KEY_FILE')"
}

getSSHPathPart() {
  echo "$(getProperty 'SSH_USER')@$(getProperty 'SSH_HOST')"
}

getSSHCmd() {
  SSH_MODE=$(getProperty 'SSH_MODE')
  if [ $SSH_MODE -eq 2 ]; then
    return 0 
  else
    return "$(getSSHCmdPart) $(getSSHPathPart)"
  fi
}

isValidFile() {
  DIR=$(trim $1)
  if [ -n "$DIR" ] && [ -f $DIR ]; then
    return 0
  else
    return 1
  fi
}

isValidDir() {
  DIR=$(trim $1)
  if [ -n "$DIR" ] && [ -d $DIR ]; then
    return 0
  else
    return 1
  fi
}

isValidRemoteDir() {
  DIR=$(trim $1)
  # TODO not working: if $($(getSSHCmd) -t -t "test -d $DIR"); then
  if isSSH; then
    return 0
  fi
  return 1
}

# $1 - date in DATE_FORMAT
# $2 - format string
formatDate() {
  if [ "$(getPlatform)" = 'Linux' ]; then
    date=$(date -d "$1 00:00" +$2)
  elif [ "$(getPlatform)" = 'FreeBSD' ]; then
    date=$(date -j -f $(getProperty 'DATE_FORMAT') $1 +$2)
  else
    echo "Unknown platform $(getPlatform), unable to format date"
  fi
  echo $date
}

dateDiff() {
  case $2 in
    -s)   sec=1;      shift;;
    -m)   sec=60;     shift;;
    -h)   sec=3600;   shift;;
    -d)   sec=86400;  shift;;
    *)    sec=86400;;
  esac
  echo $((($(date +%s) - $(formatDate $1 "%s")) / sec))
}

monthDiff() {
  YEAR_DIFF=$(($(date +%Y) - $(formatDate $1 "%Y")))
  MONTH_DIFF=$(($(date +%m) - $(formatDate $1 "%m")))
  echo $(($YEAR_DIFF * 12 + $MONTH_DIFF))
}
