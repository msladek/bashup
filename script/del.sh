#!/bin/bash
. ./funcs.sh

keepDay() {
  KEEP=$(getProperty 'KEEP_BACKUPS_DAYS')
  if [ $(dateDiff $1) -lt $KEEP ]; then
    return 0
  fi
  return 1;
}

keepWeek() {
  KEEP=$(getProperty 'KEEP_BACKUPS_WEEKS')
  WEEKDAY=$(getProperty 'KEEP_BACKUPS_WEEK_DAY')
  if [ $(formatDate $1 "%w") -eq $WEEKDAY ] && [ $(dateDiff $1) -lt $(($KEEP * 7)) ]; then
    return 0
  fi
  return 1
}

keepMonth() {
  KEEP=$(getProperty 'KEEP_BACKUPS_MONTHS')
  MONTHDAY=$(getProperty 'KEEP_BACKUPS_MONTH_DAY')
  if [ $(formatDate $1 "%d") -eq $MONTHDAY ] && [ $(monthDiff $1) -lt $KEEP ]; then
    return 0
  fi
  return 1
}

check() {
  if keepDay $1 || keepWeek $1 || keepMonth $1; then
    return 1
  else
    return 0
  fi
}

if [ $(getProperty 'ACTIVATE_DEL') -eq 0 ]; then
  BACKUP_DIR=$(getProperty 'BACKUP_DIR')
  echo "Deleting expired backups ..."
  # TODO -name not working if format is changed (also in backup.sh)
  for DIR in $(find $BACKUP_DIR -maxdepth 1 -type d -name '[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]' | sort -r); do
    DIR=$(basename $DIR)
    if check $DIR; then
      echo "  deleting $BACKUP_DIR/$DIR"
      rm -rf $BACKUP_DIR/$DIR
    else
      debug "keeping $DIR"
    fi
  done
  echo "... completed!"
fi

exit 0
