#!/bin/bash
cd "$( cd "$( dirname "$0" )" && pwd )"
. ./funcs.sh

# gets last backup dir
getLastBackupDir() {
  BACKUP_DIR=$(getProperty 'BACKUP_DIR')
  # TODO -name not working if format is changed (also in backup_del.sh)
  for DIR in $(find $BACKUP_DIR -maxdepth 1 -type d -name '[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]' | sort -r); do
    DIR=$(basename $DIR)
    if [ $(dateDiff $DIR) -gt 0 ]; then
      echo $BACKUP_DIR/$DIR
      break
    fi
  done
}

# deletes old temp dir and creates new one
# $1 - temp dir
setup() {
  if rm -rf "$1" && mkdir -p "$1"; then
    debug "created $1"
    return 0
  else
    echo "ERROR: Unable to remove/create temp directory $1"
    return 1
  fi
}

# renames temp dir and sets read only
# $1 - temp dir
# $2 - backup dir name
deploy() {
  if mv -f "$1" "$2"; then
    debug "renamed $1 to $2"
    RIGHTS=$(getProperty 'BACKUP_FOLDER_RIGHTS')
    if [ "$RIGHTS" -eq "$RIGHTS" ] 2>/dev/null; then
      echo "Restricting file access ..."
      find $2 -type d | grep -v "^.$" | xargs chmod $RIGHTS
      echo "... done!"
    fi
    return 0
  else
    echo "ERROR: Unable to rename temp directory $1 to $1"
    return 1
  fi
}

CUR_DIR=$(getProperty 'BACKUP_DIR')/$(date +"$(getProperty 'DATE_FORMAT')")
echo "Backup destination: $CUR_DIR"

TEMP_DIR=$(getProperty "BACKUP_DIR")/temp
DIRS_SUBDIR=$TEMP_DIR
LAST_BACKUP_DIR=$(getLastBackupDir)

if [ ! -d $CUR_DIR ]; then
  if setup $TEMP_DIR && 
     sh ./dirs.sh $DIRS_SUBDIR $LAST_BACKUP_DIR && 
     deploy $TEMP_DIR $CUR_DIR; then
    sh ./del.sh
  else
    exit 1
  fi
else
  echo "Backup has already been executed today, try again another day"
  exit 1
fi

exit 0
