#!/bin/bash
. ./funcs.sh

getPriority() {
  PRIORITY=$(getProperty 'COPY_PRIORITY')
  if [ -n "$PRIORITY" ] && [ "$PRIORITY" -eq "$PRIORITY" ] 2>/dev/null; then
    echo "nice -n $PRIORITY"
  fi
}

getLimit() {
  SPEED=$(getProperty 'MAX_COPY_SPEED')
  if [ $SPEED -gt 0 ]; then
    echo "--bwlimit=$SPEED"
  fi
}

cprsync() {
  SOURCE_DIR=$1
  DEST_DIR=$2
  LAST_BACKUP_SPEC_DIR=$3/$(basename $SOURCE_DIR)
  if isValidDir $LAST_BACKUP_SPEC_DIR; then
    debug "Creating hardlinks from $LAST_BACKUP_SPEC_DIR"
    cp -al $LAST_BACKUP_SPEC_DIR $DEST_DIR
  fi
  if ! isSSH && $(getPriority) rsync --delete $(getLimit) -a $SOURCE_DIR $DEST_DIR; then
    return 0
  elif $(getPriority) rsync --delete $(getLimit) -aze "$(getSSHCmdPart)" "$(getSSHPathPart):$SOURCE_DIR" $DEST_DIR; then
    return 0
  fi
  return 1
}

# reads out given list and run backup for each valid directory
# $1 - path to list
# $2 - destination dir
# $3 - last backup dir (if any)
run() {
  LIST_FILE=$1
  DEST_DIR=$2
  LAST_BACKUP_DIR=$3
  if isValidFile $LIST_FILE; then
    while read SOURCE_DIR; do
      SOURCE_DIR=$(trim $SOURCE_DIR)
      if isValidDir $SOURCE_DIR || isValidRemoteDir $SOURCE_DIR; then
        # TODO remove trailing / from SOURCE_DIR
        echo "  $SOURCE_DIR ..."
        if ! cprsync $SOURCE_DIR $DEST_DIR $LAST_BACKUP_DIR; then
          warn "Unable to backup $SOURCE_DIR"
        fi
      else
        debug "Invalid backup source directory: $SOURCE_DIR"
      fi
    done < $LIST_FILE
  else
    debug "Invalid list file: $LIST_FILE"
  fi
}


DEST_DIR=$1
LAST_BACKUP_DIR=$2
mkdir -p "$DEST_DIR"
echo "Backing up directories ..."
run $(getProperty 'BACKUP_LIST_DIRS') $DEST_DIR $LAST_BACKUP_DIR
echo "... completed!"

exit 0;
